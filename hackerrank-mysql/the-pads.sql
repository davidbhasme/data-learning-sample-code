/*
** Problem: https://www.hackerrank.com/challenges/the-pads/problem
*/

SELECT
    CONCAT(Name, "(", UPPER(LEFT(Occupation, 1)), ")") as Detail

FROM
    Occupations

ORDER BY
    Name;

SELECT
    REPLACE(
        REPLACE(
            "There are a total of [occupation_count] [occupation]s.",
            "[occupation_count]", Name_Count),
        "[occupation]", Lower_Occupation)
FROM (
  SELECT
    LOWER(Occupation) AS Lower_Occupation,
    Count(Name) AS Name_Count
  FROM
    Occupations
  GROUP BY
    Occupation
) AS Occupation_Summary

ORDER BY
  Name_Count, Lower_Occupation
