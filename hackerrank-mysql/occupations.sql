/*
**   Problem: https://www.hackerrank.com/challenges/occupations/problem
*/

SET @doctorRow = 0, @professorRow = 0, @singerRow = 0, @actorRow = 0;

SELECT
  MIN(Doctor), MIN(Professor), MIN(Singer), MIN(Actor)
FROM (
  SELECT
    CASE Occupation
      WHEN 'Doctor' THEN @doctorRow := @doctorRow + 1
      WHEN 'Professor' THEN @professorRow := @professorRow + 1
      WHEN 'Singer' THEN @singerRow := @singerRow + 1
      WHEN 'Actor' THEN @actorRow := @actorRow + 1
    END AS row,
    IF(Occupation = 'Doctor', Name, NULL) AS Doctor,
    IF(Occupation = 'Professor', Name, NULL) AS Professor,
    IF(Occupation = 'Singer', Name, NULL) AS Singer,
    IF(Occupation = 'Actor', Name, NULL) AS Actor
  FROM
      Occupations
  ORDER BY Name
) AS pivoted
GROUP BY
  row
