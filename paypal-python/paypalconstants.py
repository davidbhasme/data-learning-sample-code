# -*- coding: utf-8 -*-
"""
@author: David Bhasme
"""

ISO_DATETIMEFORMAT = "%Y-%m-%dT%H:%M:%S"
MAX_PAGE_SIZE = 500
OAUTH_TOKEN_GENERATION_PATH = "/v1/oauth2/token"
PAYPAL_API_URL = "https://api.paypal.com"
PAYPAL_API_SANDBOX_URL = "https://api.sandbox.paypal.com"
REPORTING_PATH = "/v1/reporting/transactions"
SCOPE_URL_INVOICING = "https://uri.paypal.com/services/invoicing"
SCOPE_URL_REPORTING = "https://uri.paypal.com/services/reporting/search/read"
SCOPE_URL_WEBHOOKS = "https://uri.paypal.com/services/applications/webhooks"
