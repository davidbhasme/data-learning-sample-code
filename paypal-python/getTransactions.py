# -*- coding: utf-8 -*-
"""
@author: David Bhasme
"""
from datetime import date
import pandas as pd
from pandas.io.json import json_normalize
import paypalrest


def add_year_column(df, from_col='transaction_info_transaction_updated_date'):
    if from_col in df:
        df['year'] = df[from_col].apply(lambda x: x.split('-')[0])
    return df


def add_month_column(df, from_col='transaction_info_transaction_updated_date'):
    if from_col in df:
        df['month'] = df[from_col].apply(lambda x: x.split('-')[1])
    return df


def get_cart_info_item_code(data):
    try:
        __code = str(data[0]['item_code'])
    except:
        __code = ''
    return __code


def get_cart_info(df):
    if 'cart_info_item_details' in df:
        df['cart_info_item_code'] = \
            df.cart_info_item_details.apply(
                lambda x: get_cart_info_item_code(x)
            )
    else:
        df['cart_info_item_code'] = ""

    return df


def get_df_subset(df, columns=[]):
    __subset = pd.DataFrame()
    for c in columns:
        if c in df:
            __subset[c] = df[c].values
        else:
            __subset[c] = ""

    if not columns:
        __subset = df

    return __subset


def get_file_length(file_name):
    try:
        f = open(file_name)
        contents = f.read()
        f.close()
    except FileNotFoundError as e:
        print(e)
        contents = ""

    return len(contents)


def get_transaction_status(df):
    if 'transaction_info_transaction_status' in df:
        paypal_status_codes = pd.DataFrame(
            {'transaction_info_transaction_status': ['S', 'P'],
             'transaction_info_status_detail': ['Success', 'Pending']})

        df = df.join(
            paypal_status_codes.set_index(
                'transaction_info_transaction_status'),
            on='transaction_info_transaction_status')
    else:
        df['transaction_info_transaction_status'] = ""
        df['transaction_info_status_detail'] = ""

    return df


def export_to_csv(df, cols, file_name):
    __months_out = 0
    __header = False if get_file_length(file_name) > 0 else True

    for __mnth in df:
        __info_df = json_normalize(__mnth, sep="_")

        if not __info_df.empty:
            __months_out += 1

            __info_df = get_cart_info(__info_df)
            __info_df = get_transaction_status(__info_df)
            __info_df = add_year_column(__info_df)
            __info_df = add_month_column(__info_df)
            __info_df = get_df_subset(__info_df, cols)

            __sub = __info_df

            if 'transaction_info_transaction_id' in __info_df:
                __sub.set_index('transaction_info_transaction_id', inplace=True)

            __csv_path = file_name

            if __months_out > 1:
                __header = False

            try:
                with open(__csv_path, 'a') as _existing_csv_file:
                    info_csv = __sub.to_csv(_existing_csv_file, header=__header)
                    _existing_csv_file.flush()
            except FileNotFoundError:
                print('Unable to write to file ' + str(__csv_path))

            __info_df = pd.DataFrame()
            __sub = pd.DataFrame()

    return __months_out


columns_wanted = [
    'year',
    'month',
    'transaction_info_transaction_id',
    'transaction_info_transaction_updated_date',
    'transaction_info_transaction_subject',
    'payer_info_email_address',
    'payer_info_payer_name_given_name',
    'payer_info_payer_name_surname',
    'cart_info_item_code',
    'transaction_info_transaction_event_code',
    'transaction_info_transaction_amount_currency_code',
    'transaction_info_transaction_amount_value',
    'transaction_info_transaction_note',
    'transaction_info_status_detail',
    'transaction_info_fee_amount_currency_code',
    'transaction_info_fee_amount_value',
    'transaction_info_available_balance_currency_code',
    'transaction_info_available_balance_value',
    'transaction_info_bank_reference_id',
    'transaction_info_ending_balance_currency_code',
    'transaction_info_ending_balance_value'
]

ISO_DATETIMEFORMAT = "%Y-%m-%dT%H:%M:%S"

csv_file = 'out.csv'
report_start_date = date(2016, 5, 1)

paypal = paypalrest.Paypal.from_json_credentials(
    'credentials/paypal_client1_live.json')

transactions = paypal.yield_monthly_transactions_since(report_start_date)

result = export_to_csv(transactions, columns_wanted, csv_file)

print(str(result) + " months published to " + csv_file)
