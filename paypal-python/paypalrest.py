# -*- coding: utf-8 -*-
"""
@author: David Bhasme
"""
from datetime import date
from datetime import datetime
from datetime import timedelta
import json
import paypalconstants as constants
import requests


def iterate_by_month(start_date, end_date):
    dt = start_date
    while dt < end_date:
        next_date = date(dt.year, dt.month, 1)
        next_date = next_date + timedelta(days=32)
        next_date = date(next_date.year, next_date.month, 1)

        yield min(next_date, end_date)
        dt = next_date


def paypal_date(raw_date, tz_offset):
    dt = datetime(raw_date.year, raw_date.month, raw_date.day, 0, 0, 0)
    dt = dt.strftime("%Y-%m-%dT%H:%M:%S") + tz_offset

    return dt


class Paypal:
    def __init__(self, client_id, client_secret, mode='sandbox'):
        self.base_url = constants.PAYPAL_API_SANDBOX_URL
        self.client_id = client_id
        self.client_secret = client_secret
        self.mode = mode
        self.token = None
        self.scopes = None

    @property
    def authorize(self):
        __base_url = constants.PAYPAL_API_URL
        if self.mode == "sandbox":
            __base_url = constants.PAYPAL_API_SANDBOX_URL

        __authUrl = __base_url + constants.OAUTH_TOKEN_GENERATION_PATH
        __headers = {'Accept-Languages': 'en-US',
                     'Content-Type': 'application/json'}
        __payload = "grant_type=client_credentials"
        __pwd = self.client_secret
        __user = self.client_id

        print("Authorizing " + __base_url)
        __response = requests.post(url=__authUrl, headers=__headers,
                                   auth=(__user, __pwd), data=__payload)

        if __response.reason == "OK":
            print("Authorized successfully")

        __response_json = __response.json()

        self.base_url = __base_url
        self.token = __response_json['access_token']
        self.scopes = __response_json['scope']

        return __response_json

    @classmethod
    def from_json_credentials(cls, json_file):
        with open(json_file) as f:
            __credentials_json = json.load(f)

            return cls(__credentials_json['client_id'],
                       __credentials_json['client_secret'],
                       __credentials_json['mode'])

    def get_transactions_by_query(self, parameters):

        if self.token is None:
            print('Generating Access Token')
            self.authorize

        if constants.SCOPE_URL_REPORTING not in self.scopes:
            raise Exception(
                'Reporting not included in scoped authorization: ' +
                self.scopes)

        __api_url_reporting = self.base_url + constants.REPORTING_PATH
        __headers = {
            'Accept-Languages': 'en-US',
            'Authorization': 'Bearer ' + self.token,
            'Content-Type': 'application/json'}
        __page = parameters['page']
        __transactions = []

        __response = requests.get(__api_url_reporting, headers=__headers,
                                  params=parameters)

        if __response.reason == "OK":
            __response_json = __response.json()
            __total_items = __response_json['total_items']
            __total_pages = __response_json['total_pages']
            __transactions = __response_json['transaction_details']

            if 0 < len(__transactions) < __total_items:
                print(' | transactions retrieved: ' + str(len(__transactions)) +
                      ' | total items: ' + str(__total_items) +
                      ' | total_pages: ' + str(__total_pages) +
                      ' | \n')
                __page += 1
                parameters['page'] = __page
                __transactions.extend(
                    self.get_transactions_by_query(parameters))

        return __transactions

    def yield_monthly_transactions_since(self, since_date=date(2019, 1, 1)):
        __results_list = []
        __tz_offset = '-07:00'
        __start_date = since_date
        __end_date = date.today()

        for __dt in iterate_by_month(__start_date, __end_date):
            __next_date = __start_date
            __params = {'end_date': paypal_date(__dt, __tz_offset),
                        'fields': 'all',
                        'page': 1,
                        'page_size': constants.MAX_PAGE_SIZE,
                        'start_date': paypal_date(__next_date, __tz_offset)}
            print('Getting transactions from ' + str(__next_date) + " to " +
                  str(__dt))

            __results_list.extend(self.get_transactions_by_query(__params))
            __start_date = __dt

            yield __results_list
