/*
 * Problem:
 * https://www.hackerrank.com/challenges/grading
 */

import java.util.*

fun gradingStudents(grades: Array<Int>): List<Int> {
    var roundedGrades = grades.map {  x ->
        when {
            (x >= 38 && (x.rem(5) > 2)) -> x + ( 5 - x.rem(5))
            else  -> x
        }
    }
    return roundedGrades
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    val grades = Array<Int>(n, { 0 })
    for (gradesItr in 0 until n) {
        val gradesItem = scan.nextLine().trim().toInt()
        grades[gradesItr] = gradesItem
    }

    val result = gradingStudents(grades)

    println(result.joinToString("\n"))

}
