/*
 * Problem: https://www.hackerrank.com/challenges/the-hurdle-race
 */

import java.util.*
import kotlin.math.max

fun hurdleRace(k: Int, height: Array<Int>): Int {
	return max(height.max()!!.toInt() - k, 0)
}

fun main(args: Array<String>) {
	val scan = Scanner(System.`in`)

	val nk = scan.nextLine().split(" ")
//	val numberOfHurdles = nk[0].trim().toInt()
	val k = nk[1].trim().toInt()

	val height = scan.nextLine().split(" ").map { it.toInt() }.toTypedArray()
	val result = hurdleRace(k, height)

	println(result)
}