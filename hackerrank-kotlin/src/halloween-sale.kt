/*
 * Problem: https://www.hackerrank.com/challenges/halloween-sale
 */

import java.util.*
import kotlin.system.measureTimeMillis


fun arithmeticSeriesSum(n: Int, seed: Int, diff: Int): Int {
	return n*(2*seed - (n - 1)* diff)/2
}

fun howManyGames2(p: Int, d: Int, m: Int, s: Int): Int {
	var p = p
	var s = s
	var games = 0
	while (s >= p) {
		s -= p
		games++
		p = Math.max(p - d, m)
	}
	return games
}

fun howManyGames(
		price: Int, discount: Int, minPrice: Int, startingBalance: Int): Int {
	var gameCounter = 0

	if (startingBalance >= price) {
		val availableDiscountCount = (price - minPrice)/discount
		var walletBalance = startingBalance
		var maxBuyCount = availableDiscountCount + 1

		while (maxBuyCount  > 0) {
			var discountSum =
					arithmeticSeriesSum(maxBuyCount, price, discount)

			if (walletBalance >= discountSum) {
				walletBalance -= discountSum
				gameCounter = maxBuyCount
				maxBuyCount = 0
			}
			maxBuyCount--
		}

		if (gameCounter >= availableDiscountCount) {
			gameCounter += walletBalance / minPrice
		}
	}

	return gameCounter
}


fun main(args: Array<String>) {
	val scan = Scanner(System.`in`)

	val testCount = scan.nextLine().trim().toInt()

	for (i in 0 until testCount) {

			val (p, d, m, s) =
					scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
		val time = measureTimeMillis {
			val answer = howManyGames(p, d, m, s)
			println(answer)
		}

		val time2 = measureTimeMillis {
			val answer2 = howManyGames2(p, d, m, s)
			println(answer2)
		}

		println("O(" + ((p-m)/d) + ") took " + time + "ms")
		println("O(" + (s) + ") took " + time2 + "ms")


	}
}
