/*
 * Problem:
 * https://www.hackerrank.com/challenges/diagonal-difference
 */

import java.util.*
import kotlin.math.abs

fun diagonalDifference(arr: Array<Array<Int>>): Int {
    val matrixSize = arr.size
    var diag1 = 0
    var diag2 = 0

    arr.forEachIndexed{ col, row ->
        diag1 += row[col]
        diag2 += row[matrixSize - col - 1]
    }
    return abs(diag1 - diag2)
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    val arr = Array<Array<Int>>(n, { Array<Int>(n, { 0 }) })

    for (i in 0 until n) {
        arr[i] = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()
    }

    val result = diagonalDifference(arr)

    println(result)
}
