/*
 * Problem: https://www.hackerrank.com/challenges/the-birthday-bar
 */

import java.util.*

fun birthday(squares: Array<Int>, day: Int, month: Int ): Int {
	var counter = 0

	for (i in 0 until squares.size + 1 - month) {

		val segment = squares.slice(IntRange(i, i + month - 1)).sum()

		if (segment == day) {
			counter ++
		}
	}

	return counter
}

fun main(args: Array<String>) {
	val scan = Scanner(System.`in`)

	val n = scan.nextLine().trim().toInt()

	val s = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

	val (d, m) =
			scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

	val result = birthday(s, d, m)

	println(result)
}