/*
 * Problem: https://www.hackerrank.com/challenges/beautiful-days-at-the-movies
 */

import java.util.*

fun reverseInt(i: Int): Int {
	return i.toString().reversed().toInt()
}

fun beautifulDays(i: Int, j: Int, k: Int): Int {
	val days = i..j

	val beautifulDays = days.filter { (it - reverseInt(it)) % k == 0 }

	return beautifulDays.size
}
fun main(args: Array<String>){
	val scan = Scanner(System.`in`)

	val (i, j, k) =
			scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

	val result = beautifulDays(i, j, k)

	println(result)
}