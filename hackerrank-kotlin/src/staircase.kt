/*
 * Problem:
 * https://www.hackerrank.com/challenges/staircase
 */

import java.util.*

fun staircase(n: Int): Unit {
	val linebreak = "\n"
	val space = " "
	val step = "#"

	var stairs = ""

	for (i in 1..n){
		if(i != n) {
			stairs += space.repeat((n - i))
			stairs += step.repeat(i)
			stairs += linebreak
		}
	}

	stairs += step.repeat(n)

	print(stairs)
}


fun main(args: Array<String>) {
	val scan = Scanner(System.`in`)

	val n = scan.nextLine().trim().toInt()

	staircase(n)
}
