import java.util.*
import kotlin.math.max

fun hourglassSum(arr: Array<Array<Int>>): Int {
    var maxSum: Int = Int.MIN_VALUE

    for (i in 0 until (arr.size - 2)) {
        for (j in 0 until (arr[i].size - 2)) {
            val sum = arr[i][j] + arr[i][j+1] + arr[i][j+2] +
                    arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2]
            maxSum = max(maxSum, sum)
        }
    }
    return maxSum
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val arr = Array<Array<Int>>(6, { Array<Int>(6, { 0 }) })

    for (i in 0 until 6) {
        arr[i] = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()
    }

    val result = hourglassSum(arr)

    println(result)

}
