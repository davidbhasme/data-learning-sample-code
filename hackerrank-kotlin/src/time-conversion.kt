/*
 * Problem
 * https://www.hackerrank.com/challenges/time-conversion
 */

import java.util.*

fun timeConversion(s: String): String {

    var convertedString = s.replace(Regex("[A-Z,a-z]"),"")
    val splitTime = convertedString.split(":")
    val minuteInt = splitTime[1].toInt()
    val secondInt = splitTime[2].toInt()

    var hourInt = splitTime[0].toInt()

    if(s.contains("PM", ignoreCase = false)) {

        hourInt = if( hourInt==12 ) 12 else hourInt + 12

    } else if( hourInt==12) {
        hourInt = 0
    }

    val hh = String.format("%02d", hourInt)
    val mm = String.format("%02d", minuteInt)
    val ss = String.format("%02d", secondInt)

    return "$hh:$mm:$ss"
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val s = scan.nextLine()

    val result = timeConversion(s)

    println(result)
}
