/*
 * Problem: https://www.hackerrank.com/challenges/breaking-best-and-worst-records
 */

import java.util.*

fun breakingRecords(scores: Array<Int>): Array<Int> {
	var highCount = 0
	var lowCount = 0

	var high = scores.first()
	var low = scores.first()

	scores.forEach {
		if ( it > high ) {
			high = it
			highCount ++
		} else if ( it < low ) {
			low = it
			lowCount ++
		}
	}
	return arrayOf(highCount, lowCount)
}

fun main(args: Array<String>) {
	val scan = Scanner(System.`in`)

	val scoreCount = scan.nextLine().trim().toInt()

	val scores = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

	val result = breakingRecords(scores)

	println(result.joinToString(" "))

}