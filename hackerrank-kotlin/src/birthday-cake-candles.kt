/*
 * Problem
 * https://www.hackerrank.com/challenges/birthday-cake-candles
 */

 import java.util.*

 fun birthdayCakeCandles(ar: Array<Int>): Int {
     var maxHeight = 0
     var maxHeightCount = 0
     ar.forEach {
         when {
             it > maxHeight -> {
                 maxHeight = it
                 maxHeightCount =  1
             }
             it == maxHeight -> maxHeightCount ++
         }
     }

     return maxHeightCount
 }

 fun main(args: Array<String>) {
     val scan = Scanner(System.`in`)

     val arCount = scan.nextLine().trim().toInt()

     val ar = scan.nextLine().split(" ").map{ it ->
         it.trim().toInt() }.toTypedArray()

     val result = birthdayCakeCandles(ar)

     println(result)
 }
