/*
 * Problem
 * https://www.hackerrank.com/challenges/apple-and-orange
 */

import java.util.*

fun countApplesAndOranges(
    houseStartPosition: Int, houseEndPosition: Int, appleTreePosition: Int, orangeTreePosition: Int, apples: Array<Int>,
    oranges: Array<Int>): Unit {

    val applePositions = countFruitsOnHouse(apples, appleTreePosition, houseStartPosition, houseEndPosition)
    val orangePositions = countFruitsOnHouse(oranges, orangeTreePosition, houseStartPosition, houseEndPosition)

    println(applePositions)
    println(orangePositions)

}

fun absolutePosition(relativePosition: Int, treePosition: Int): Int{
    return relativePosition + treePosition
}

fun countFruitsOnHouse(
    fruitPositions: Array<Int>, fruitTreePosition: Int, houseStartPosition: Int, houseEndPosition: Int): Int{

    var fruitCount = 0
    fruitPositions.forEach{ fruit ->
        when {
            (absolutePosition(fruit, fruitTreePosition) in (houseStartPosition..houseEndPosition)) -> fruitCount ++
        }
    }
    return fruitCount
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val st = scan.nextLine().split(" ")

    val s = st[0].trim().toInt()

    val t = st[1].trim().toInt()

    val ab = scan.nextLine().split(" ")

    val a = ab[0].trim().toInt()

    val b = ab[1].trim().toInt()

    val mn = scan.nextLine().split(" ")

    val m = mn[0].trim().toInt()

    val n = mn[1].trim().toInt()

    val apples = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    val oranges = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    countApplesAndOranges(s, t, a, b, apples, oranges)
}
