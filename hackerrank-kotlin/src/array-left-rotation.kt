import java.util.*

fun rotateArray(d: Int, arr: Array<Int>): String {
    return (arr.takeLast(arr.size - d) + arr.take(d))
            .joinToString(separator = " ")
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val nd = scan.nextLine().split(" ")

    val n = nd[0].trim().toInt()

    val d = nd[1].trim().toInt()

    val a = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    print(rotateArray(d, a))
}
