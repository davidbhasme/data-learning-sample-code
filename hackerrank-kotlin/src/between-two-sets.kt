/*
 * Problem:
 * https://www.hackerrank.com/challenges/between-two-sets
 */

import java.util.*

fun getTotalX(a: Array<Int>, b: Array<Int>): Int {
    val c = b.sortedArray()

    val lower: Int = lcm(a)

    var total = 0
    var skip = false

    for(i in 0 until c.size-1){
        if (lower > (c[i+1] - c[i])) {
            skip = true
        }
    }

    if (skip == false) {

        val upper: Int = gcd(c)

        generateSequence(lower) {
            (it + lower)
        }.takeWhile {
            (it <= upper)
        }.forEach {
            if (upper % it == 0) {
                total++
            }
        }
    }

    return total
}

fun gcd(n: Int, m: Int): Int {
    var gcd = 1
    var i = 1

    while(i <= n && i <= m) {

        if( n % i == 0 && m % i == 0 ) {
            gcd = i
        }

        i ++
    }

    return gcd
}

fun gcd(a: Array<Int>): Int {
    var result = a[0]

    a.forEach{
        if( result != it) {
            result = gcd(result, it)
        }
    }

    return result
}

fun lcm(n: Int, m: Int): Int {
    return n * (m / gcd(n, m))
}

fun lcm(a: Array<Int>): Int {
    var result = a[0]

    a.forEach{
        if( result != it) {
            result = lcm(result, it)
        }
    }

    return result
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val nm = scan.nextLine().split(" ")

    val n = nm[0].trim().toInt()

    val m = nm[1].trim().toInt()

    val a = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    val b = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    val total = getTotalX(a, b)

    println(total)
}
