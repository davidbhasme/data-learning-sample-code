/*
 * Problem: https://www.hackerrank.com/challenges/designer-pdf-viewer
 */

import java.util.*
import kotlin.math.max

fun designerPdfViewer(height: Array<Int>, word: String): Int {
	var maxHeight = 0

	word.forEach {
		val heightIndex = (it.toByte().toInt() - 'a'.toByte().toInt())

		maxHeight = max(height[heightIndex], maxHeight)
	}

	return maxHeight * word.length
}

fun main(args: Array<String>){
	val scan = Scanner(System.`in`)

	val height =
			scan.nextLine()
			.split(" ").map { it.trim().toInt() }.toTypedArray()

	val word = scan.nextLine()

	val result = designerPdfViewer(height, word)

	println(result)
}