/*
 * Problem: https://www.hackerrank.com/challenges/angry-professor
 */

import java.util.*

fun angryProfessor(
		expectedStudents: Int, studentArrivalTimes: Array<Int>): String {
	var classCancelled = "NO"
	var studentCounter = 0
	var studentsOnTime = 0

	val totalStudentCount = studentArrivalTimes.size

	while (studentsOnTime < expectedStudents
			&& studentCounter < totalStudentCount) {

		if (studentArrivalTimes[studentCounter] <= 0) {
			studentsOnTime++
		}

		studentCounter ++
	}

	if ( studentsOnTime < expectedStudents) {
		classCancelled = "YES"
	}

	return classCancelled
}

fun main(args: Array<String>) {
	val scan = Scanner(System.`in`)

	val testCount = scan.nextLine().trim().toInt()

	for (t in 0 until testCount) {
		val (n, k) =
			scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

		val a =
			scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

		val result = angryProfessor(k, a)

		println(result)
	}
}