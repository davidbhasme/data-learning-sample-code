/*
 * Problem: https://www.hackerrank.com/challenges/utopian-tree
 */

import java.util.*

fun treeGrowth(): Sequence<Int> = sequence {
	var elements = Pair(1,2)

	while (true) {
		yield(elements.first)
		elements = Pair(elements.second,
				if (elements.second % 2 == 0) {
					elements.second + 1
				} else {
					elements.second * 2
				})
	}
}

fun utopianTree(n: Int): Int {

	return treeGrowth().elementAt(n)
}

fun main(args: Array<String>) {
	val scan = Scanner(System.`in`)

	val testCount = scan.nextLine().trim().toInt()

	for (test in 0 until testCount) {

		val n = scan.nextLine().trim().toInt()

		val result = utopianTree(n)

		println(result)

	}
}