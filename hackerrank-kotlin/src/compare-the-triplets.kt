/*
 * Problem:
 * https://www.hackerrank.com/challenges/compare-the-triplets
 */

import kotlin.collections.*
import kotlin.comparisons.*
import kotlin.io.*
import kotlin.text.*

fun compareTriplets(a: Array<Int>, b: Array<Int>): Array<Int> {
	var scores: Array<Int> = Array(2) {0}

	a.forEachIndexed{ index, i ->
		val diff = compareValues(i, b[index])
		when {
			diff > 0 -> scores[0] ++
			diff < 0 -> scores[1] ++
		}
	}
	return scores
}

fun main(args: Array<String>) {
	val a = readLine()!!.trimEnd().split(" ").map{ it.toInt() }.toTypedArray()

	val b = readLine()!!.trimEnd().split(" ").map{ it.toInt() }.toTypedArray()

	val result = compareTriplets(a, b)

	println(result.joinToString(" "))
}
