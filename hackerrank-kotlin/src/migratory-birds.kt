/*
 * Problem: https://www.hackerrank.com/challenges/migratory-birds
 */

import java.util.*

fun migratoryBirds(arr: Array<Int>): Int {
	val frequencies = arr.groupingBy { it }.eachCount()

	return frequencies.maxBy { it.value }?.key ?: 0
}

fun main(args: Array<String>) {
	val scan = Scanner(System.`in`)

	val n = scan.nextLine().trim().toInt()

	val arr =
			scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

	val result = migratoryBirds(arr)

	println(result)
}