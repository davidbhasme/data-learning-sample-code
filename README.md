# _David Bhasme_: Empowering Data Professionals

I've spent a good portion of [my career](https://linkedin.com/in/davidbhasme) wrangling and presenting data.

This repo will include:
1. examples of my code as a data analyst
2. practice [hackerrank](https://www.hackerrank.com/profile/dbhasme)
submissions [^2]
3. snippets from my efforts to hone various data management skills.

The Skills and Technologies charts below serve as a reference about my current
strengths (top of the charts), as well as an indicator for areas I expect to
grow (bottom of the charts):

### Data Management Skills
| Skill | Experience |
| --- | --- |
| Data Wrangling | ########## |
| Communication | ########## |
| Git | ######### |
| Project Mgt | ######### |
| Programming | ######## |
| Team Collab | ######## |
| Data Visualization | ######## |
| Data Warehousing | ####### |
| Software Architecture | ####### |
| Statistics | ##### |
| Machine Learning | ## |

### Technologies
| Technology | Experience |
| ---- |:--- |
| SQL | ########## |
| Python | ######## |
| Docker | ###### |
| BigQuery | ##### |
| Airflow | ##### |
| Google Cloud Composer | #### |
| Java | #### |
| Kotlin | ### |

### Links
[^1]: More about my career: https://linkedin.com/in/dbhasme
[^2]: HackerRank profile: https://hackerrank.com/profile/dbhasme
